#!/usr/bin/env python3


import scrapy


class FramalibreSpider(scrapy.Spider):
    name = "Framalibre"
    start_urls = ['https://framalibre.org/annuaires/métiers',
                      'https://framalibre.org/annuaires/cms',
                      'https://framalibre.org/annuaires/bureautique'
                      'https://framalibre.org/annuaires/cloudwebapps',
                      'https://framalibre.org/annuaires/création',
                      'https://framalibre.org/annuaires/développement',
                      'https://framalibre.org/annuaires/éducation',
                      'https://framalibre.org/annuaires/internet',
                      'https://framalibre.org/annuaires/jeux',
                      'https://framalibre.org/annuaires/multimédia',
                      'https://framalibre.org/annuaires/science',
                      'https://framalibre.org/annuaires/sécurité',
                      'https://framalibre.org/annuaires/système',
                      ]


    def parse(self, response):
        titre = response.xpath('//meta[@property="og:title"]/@content').extract_first()
        url = response.xpath('//meta[@property="og:url"]/@content').extract_first()
        ident = url.split('/')[-1]
        description = response.xpath('//meta[@property="og:description"]/@content').extract_first()
        wikipedia_fr = None
        wikipedia_divs = response.css('div.field-name-field-page-wikip-dia div div a')
        if wikipedia_divs:
            wikipedia_fr = wikipedia_divs[0].xpath('@href').extract()[0]
        if 'content' in url:
            yield {'id' : ident, 'titre' : titre, 'url' : url, 'description' : description, 'wikipediafr' : wikipedia_fr }
        liens_interessants = response.xpath('//a[starts-with(@href, "/content/")]/@href').extract()
        lien_next = []
        li_next = response.css('li.next a')
        if li_next:
            lien_next = li_next[0].xpath('@href').extract()
        for l in liens_interessants + lien_next:
            next_page = response.urljoin(l)
            yield scrapy.Request(next_page, callback=self.parse)
