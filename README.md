Free Software Catalog Interoperability
======================================

This projects help to interoperate between free software catalogs, via Wikidata.

A few links:

https://www.wikidata.org/wiki/Wikidata:WikiProject_Informatics/Software/Properties

https://semestriel.framapad.org/p/interop-catalogues-ll

Purpose
-------
We wish to import free software catalogs like Software Heritage or Framalibre
into Wikidata as long as it is useful.

We use the Mix'nMatch tool when possible:

https://tools.wmflabs.org/mix-n-match

Installation
------------
Requirement : python scrapy

Usage
-----
/path_to/scrapy runspider framalibre_spider.py -o framalibre.json
